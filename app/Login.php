<?php 

    require_once 'config/Db.php';

    // Crea o renauda la sesión
    session_start();
    
    // Verificamos que los campos no esten vacíos
    if((!isset($_POST['email']) || trim($_POST['email']==""))|| !isset($_POST['password']) || trim($_POST['password']=="")){
        echo "<script>
                window.location= '../login.php'
                alert('Complete todos los campos porfavor');
            </script>";
    } else {

        // Validación de correo
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            echo "<script>
                window.location= '../login.php'
                alert('Correo No válido');
            </script>";
        } else {
            
            // login de usuario
            try {
                $query = 'SELECT * FROM users WHERE email = :email';
    
                $stm = $DB->prepare($query);
                $stm->execute(array(':email' => $query, ($_POST['email'])));
    
                $result = $stm->fetchAll(PDO::FETCH_ASSOC);

                $stm = null;
    
                //Si existe resultado de la consulta
                if(!empty($result)) {
                    //Comparamos contraseñas
                    if(password_verify(($_POST['password']), $result[0]['password']) && $result[0]['status'] == 1){
    
                        // Establecemos variables de sesión
                        $_SESSION['status'] = 1;    // Estado 1: Sessión activa
                        $_SESSION['name'] =  $result[0]['name'];
                        $_SESSION['email'] = $_POST['email'];
                        
                        // Redireccionamos a Home.php
                        header('location: ../home.php');
    
                    } else {
                        // Contraseña incorrecta
                        echo "<script>
                                window.location= '../login.php'
                                alert('Datos incorrectos');
                            </script>";
                    }
                } else {
                    // Mensaje de usuario no encontrado en BD
                    echo "<script>
                                alert('Usuario no registrado');
                                window.location= '../login.php'
                        </script>";
                }
    
            } catch (Exception $e) {
                print "Error: ". $e->getMessage();
            }
        }
    }
?>