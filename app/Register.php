<?php 

    require_once 'config/Db.php';
    
    // Crea o renauda la sesión
    session_start();

    // Verificamos si los campos estan vacíos
    if((!isset($_POST['name']) || trim($_POST['name']=="")) || 
        !isset($_POST['email']) || trim($_POST['email']=="") ||
        !isset($_POST['password']) || trim($_POST['password']=="") ||
        !isset($_POST['confirm_password']) || trim($_POST['confirm_password']=="")){
        echo "<script>
                window.location= '../register.php'
                alert('Complete todos los campos porfavor.');
            </script>";

    } else {

        // Validación de correo
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            echo "<script>
                window.location= '../register.php'
                alert('Correo No válido.');
            </script>";
        } else {
            // Validamos contraseña mayor a 6 caracteres
            if(strlen(trim($_POST['password'])) < 6){
                echo "<script>
                    window.location= '../register.php'
                    alert('Contraseña debe contener al menos 6 caracteres.');
                </script>";
            } else {
                // Validamos confirmación de contraseña
                if(trim($_POST['password']) != trim($_POST['confirm_password'])){
                    echo "<script>
                        window.location= '../register.php'
                        alert('Contraseñas no coinciden.');
                    </script>";
                } else {
                    // Validamos si correo ya ha sido registrado 
                    $select_query = 'SELECT * FROM users WHERE email = :email';

                    $select_stm = $DB->prepare($select_query);
                    $select_stm->execute(array(':email' => $select_query, ($_POST['email'])));

                    $result = $select_stm->fetchAll(PDO::FETCH_ASSOC);

                    $select_stm = null;

                    // Si correo ya ha sido registrado
                    if(!empty($result)) {
                        echo "<script>
                            window.location= '../register.php'
                            alert('Correo ya ha sido registrado.');
                        </script>";
                    } else {

                        // Crea nuevo usuario
                        try {
                            $insert_query = 'INSERT INTO users (name, email, password, status) VALUES (:name, :email, :password, :status)';
                
                            $insert_Stm = $DB->prepare($insert_query);
                            $insert_Stm->execute(array(
                                ':name' => $insert_query, ($_POST['name']),
                                ':email' => $insert_query, ($_POST['email']),
                                ':password' => $insert_query, (password_hash($_POST['password'], PASSWORD_DEFAULT)),
                                ':status'   => $insert_query, (1)
                            ));
                
                            $result = $insert_Stm->fetchAll(PDO::FETCH_ASSOC);

                            $insert_Stm = null;

                            // Establecemos variables de sesión
                            $_SESSION['status'] = 1;
                            $_SESSION['name'] = $_POST['name'];
                            $_SESSION['email'] = $_POST['email'];
                            
                            // Redireccionamos a home
                            header('location: ../home.php');
                
                        } catch (Exception $e) {
                            print "Error: ". $e->getMessage() . "<br/>";
                        }
                    }
                    
                }
            }
        }

    }


?>