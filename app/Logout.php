<?php
    
    require_once 'config/Db.php';
    
    // Crea o renauda la sesión
    session_start();

    session_unset();    // Eliminamos variables de sessión
    session_destroy();  // Cierra la sesión

    // Redirecciona a login
    header('location: ../login.php');

