<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="packages/bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/main.css">

    <title>Dandy Challenge</title>
</head>

<body>

    <main class="container">
        
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a href="index.php">Home</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <?php
                        if(!isset($_SESSION['status'])) {   
                    ?>
                        <li class="nav-item">
                            <a href="register.php" class="nav-link">Register</a>
                        </li>
                        <li class="nav-item">
                            <a href="login.php" class="nav-link">Login</a>
                        </li>
                    <?php
                        } else {
                    ?>
                        <li class="nav-item">
                            <a href="app/Logout.php" class="nav-link">Logout</a>
                        </li>
                    <?php

                        }
                    ?>
                </ul>
            </div>
        </nav>  