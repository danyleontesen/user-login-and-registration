<?php 

    // Crea o renauda la sesión
    session_start();

    // Si no existe variable se sesión, no hay sesión activa y redireccione a login
    if(!isset($_SESSION['status'])) {
        header('location: login.php');
    }

    require('partials/header.php');

?>

    <div class="container pt-5">
        <div class="jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">Welcome to home!!</h1>
                <p class="lead"><?php echo $_SESSION['name']; ?>.</p>
            </div>
        </div>
    </div>

<?php require('partials/footer.php') ?>