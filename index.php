<?php 
    require('partials/header.php');

    // Crea o renauda la sesión
    session_start();

    // Si existe variable se sesión, hay sesión activa y redireccione a home
    if(isset($_SESSION['status'])) {
        header('location: home.php');
        echo $_SESSION['status'];
    }
?>

    <div class="container pt-5">
        <div class="jumbotron-fluid">
            <h1 class="display-4">Hello, world!</h1>
            <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quidem at ex possimus pariatur consectetur. Quibusdam est dolorum dicta ratione natus ab, veniam sed molestias ullam laudantium accusantium quas libero..</p>
            <hr class="my-4">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quisquam dolores incidunt, repellat similique cum placeat enim minima sit quas architecto, necessitatibus facilis ad reiciendis. Vero eaque laboriosam commodi et perspiciatis!.</p>

            <a class="btn btn-primary btn-lg" href="login.php" role="button">Login</a>
        </div>
    </div>

<?php require('partials/footer.php'); ?>
