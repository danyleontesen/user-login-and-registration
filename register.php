    <?php 
    
        require('partials/header.php');
        
        // Crea o renauda la sesión
        session_start();

        // Si existe variable se sesión, hay sesión activa y redireccione a home
        if(isset($_SESSION['status'])) {
            header('location: home.php');
            echo $_SESSION['status'];
        }

        ?>

        <div class="app-content">
            <div class="register__title">
                <h3 class="text-center">Register</h3>
            </div>
            <div class="row justify-content-center">
                <form action="app/Register.php" method="POST" class="form challenge__form col-4">
                    <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control" required>
                        <label for="name">Name</label>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control" required>
                        <label for="email">Email</label>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control" required>
                        <label for="password">Password</label>
                    </div>
                    <div class="form-group">
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control" required>
                        <label for="confirm_password">Confirm Password</label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" id="btn-register">Register</button>
                    </div>
                </form>
            </div>
        </div>

    <?php require('partials/footer.php'); ?>